<?php
/**
 * Connection checker.
 *
 * @package WPDesk\DhlShippingService\DhlApi
 */

namespace WPDesk\DhlExpressShippingService\DhlApi\XmlApi;

use DHL\Client\Web;
use DHL\Datatype\AM\PieceType;
use DHL\Entity\AM\GetQuote;
use DHL\Entity\AM\GetQuoteResponse;
use Psr\Log\LoggerInterface;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\DhlExpressShippingService\DhlApi\ApiConnectionChecker;
use WPDesk\DhlExpressShippingService\DhlSettingsDefinition;

/**
 * Can check connection.
 */
class XmlApiConnectionChecker implements ApiConnectionChecker
{

	/**
	 * Settings.
	 *
	 * @var SettingsValues
	 */
	private $settings;

	/**
	 * Logger.
	 *
	 * @var LoggerInterface
	 */
	private $logger;

	/** @var bool */
	private $is_testing;

	/**
	 * ConnectionChecker constructor.
	 *
	 * @param SettingsValues  $settings .
	 * @param LoggerInterface $logger .
	 * @param bool $is_testing .
	 */
	public function __construct( SettingsValues $settings, LoggerInterface $logger, $is_testing ) {
		$this->settings         = $settings;
		$this->logger           = $logger;
		$this->is_testing       = $is_testing;
	}

	/**
	 * @param string $site_id .
	 * @param string $password .
	 *
	 * @return GetQuote
	 * @throws \Exception
	 */
	private function create_quote( $site_id, $password ) {
		$sample = new GetQuote();
		$sample->SiteID = $site_id;
		$sample->Password = $password;
		$sample->MessageTime = '2001-12-17T09:30:47-05:00';
		$sample->MessageReference = 'reference_28_to_32_chars_1234567';

		$sample->BkgDetails->Date = date('Y-m-d');
		$sample->BkgDetails->PaymentCountryCode = 'GB';
		$sample->BkgDetails->DimensionUnit = 'CM';
		$sample->BkgDetails->WeightUnit = 'KG';
		$sample->BkgDetails->ReadyTime = 'PT10H21M';
		$sample->BkgDetails->ReadyTimeGMTOffset = '+01:00';

		$piece = new PieceType();
		$piece->PieceID = 1;
		$piece->Height = 10;
		$piece->Depth = 10;
		$piece->Width = 10;
		$piece->Weight = 10;
		$sample->BkgDetails->addPiece($piece);

		$sample->From->CountryCode = 'GB';
		$sample->From->Postalcode = 'DD13JA';

		$sample->To->City = 'Herndon';
		$sample->To->Postalcode = '20171';
		$sample->To->CountryCode = 'US';
		$sample->BkgDetails->IsDutiable = 'N';

		return $sample;
	}

	/**
	 * Pings API.
	 * Throws exception on failure.
	 *
	 * @return void
	 * @throws \Exception .
	 */
	public function check_connection() {
		$mode = 'production';
		if ( $this->settings->get_value( DhlSettingsDefinition::FIELD_TESTING ) === 'yes' ) {
			$mode = 'staging';
		}
		$client = new Web( $mode );
		$xml_response = $client->call(
			$this->create_quote(
				$this->settings->get_value( DhlSettingsDefinition::FIELD_SITE_ID, '' ),
				$this->settings->get_value( DhlSettingsDefinition::FIELD_API_PASSWORD, '' )
			)
		);

		$response = new GetQuoteResponse();
		$response->initFromXML( $xml_response );;
	}

}
