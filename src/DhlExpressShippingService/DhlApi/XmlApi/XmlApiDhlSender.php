<?php

namespace WPDesk\DhlExpressShippingService\DhlApi\XmlApi;

use DHL\Client\Web;
use DHL\Entity\AM\GetQuote;
use DHL\Entity\AM\GetQuoteResponse;
use Psr\Log\LoggerInterface;
use WPDesk\DhlExpressShippingService\DhlApi\Sender;

/**
 * Send request to DHL Express API
 *
 * @package WPDesk\DhlExpressShippingService\DhlApi
 */
class XmlApiDhlSender implements Sender
{

	/**
	 * Logger
	 *
	 * @var LoggerInterface
	 */
	private $logger;


	/**
	 * Is testing?
	 *
	 * @var bool
	 */
	private $is_testing;

	/**
	 * DhlSender constructor.
	 *
	 * @param LoggerInterface $logger Logger.
	 * @param bool $is_testing Is testing?.
	 */
	public function __construct(LoggerInterface $logger, $is_testing = true)
	{
		$this->logger     = $logger;
		$this->is_testing = $is_testing;
	}

	/**
	 * Send request.
	 *
	 * @param GetQuote $request DHL request.
	 *
	 * @return GetQuoteResponse
	 *
	 * @throws \Exception
	 */
	public function send($request)
	{

		$mode = 'production';
		if ($this->is_testing) {
			$mode = 'staging';
		}

		$this->logger->info(
			'API request',
			[
				'content' => $request->toXML(),
				'mode'    => $mode,
			]
		);

		$client = new Web($mode);

		$xml_response = $client->call($request);

		$this->logger->info(
			'API response',
			[
				'content' => $xml_response,
			]
		);

		$response = new GetQuoteResponse();
		$response->initFromXML($xml_response);

		return $response;
	}

}
