[![pipeline status](https://gitlab.com/wpdesk/dhl-express-shipping-service/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/dhl-express-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/dhl-express-shipping-service/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/dhl-express-shipping-service/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/dhl-express-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/dhl-express-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/dhl-express-shipping-service/downloads)](https://packagist.org/packages/wpdesk/dhl-express-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/dhl-express-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/dhl-express-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/dhl-express-shipping-service/license)](https://packagist.org/packages/wpdesk/dhl-express-shipping-service) 

DHL Express Shipping Service
============================

