## [2.2.0] - 2024-07-19
### Fixed
- connection checker

## [2.1.2] - 2024-04-04
### Updated
- Communication in settings

## [2.1.1] - 2024-03-19
### Fixed
- Connecttion checker logger

## [2.1.0] - 2023-11-30
### Added
- REST API insurance support

## [2.0.12] - 2023-11-30
### Fixed
- default API type in connection checker

## [2.0.9] - 2023-10-03
### Fixed
- minimal package weight

## [2.0.8] - 2023-10-03
### Fixed
- Texts

## [2.0.6] - 2023-10-02
### Fixed
- Default Weight in REST API

## [2.0.4] - 2023-09-29
### Fixed
- Connection Checker in REST API

## [2.0.3] - 2023-09-28
### Fixed 
- Package defaults
- Payer account in REST API
- Weight rounding in REST API

## [2.0.0] - 2023-09-26
### Added
- REST API

## [1.7.1] - 2023-06-16
### Fixed
- Test mode

## [1.7.0] - 2023-02-14
### Added
- domestic shipping service

## [1.6.1] - 2023-02-02
### Changed
- Test mode always available

## [1.6.0] - 2022-08-30
### Added
- de_DE translations

## [1.5.1] - 2022-08-12
### Changed
- Polish translations

## [1.5.0] - 2022-04-19
### Changed
- Octolize urls

## [1.4.2] - 2022-04-12
### Changed
- Service description
- Service name

## [1.4.0] - 2022-04-11
### Removed
- Shipping Zones support

## [1.3.1] - 2022-04-08
### Added
- Shipping Zones support - texts

## [1.3.0] - 2022-04-07
### Added
- Shipping Zones support

## [1.2.1] - 2021-07-28
### Changed
- duties texts

## [1.2.0] - 2021-07-26
### Added
- duties

## [1.1.3] - 2021-01-07
### Changed
- label of status

## [1.1.2] - 2020-12-09
### Fixed
- insurance rounding

## [1.1.1] - 2020-09-07
### Changed
- description and documentation links in settings

## [1.1.0] - 2020-09-07
### Added
- documents service, service code: D

## [1.0.19] - 2020-07-01
### Fixed
- package value rounding 

## [1.0.17] - 2019-04-07
### Changed
- DHL Express services 

## [1.0.16] - 2019-04-07
### Changed
- payment account to discounted rates 

## [1.0.15] - 2019-03-27
### Added
- rounding with precision 3 on weight and dimensions 

## [1.0.14] - 2019-03-26
### Added
- minimal weight moved to constant 

## [1.0.13] - 2019-03-26
### Added
- minimal weight 

## [1.0.12] - 2019-03-26
### Fixed
- texts and translations 

## [1.0.11] - 2019-03-26
### Added
- basic packer settings 

## [1.0.10] - 2019-03-26
### Changed
- removed rate type 

## [1.0.9] - 2019-03-26
### Changed
- Payment Account Number in request

## [1.0.8] - 2019-03-26
### Changed
- Translations

## [1.0.7] - 2019-03-26
### Changed
- Payment Account Number moved to Rates Adjustments section

## [1.0.6] - 2019-03-26
### Added
- WORLDWIDE EXPRESS service D
### Changed
- Acoount number to Payment Account Number 

## [1.0.5] - 2019-03-25
### Fixed
- texts and translations 

## [1.0.4] - 2019-03-25
### Added
- added NetworkTypeCode=AL to API request 
### Fixed
- DHL services 

## [1.0.3] - 2019-03-25
### Fixed
- do not add empty rates (without currency and zero charged) 

## [1.0.2] - 2019-03-24
### Fixed
- currencies support 

## [1.0.1] - 2019-03-23
### Fixed
- docs link

## [1.0.0] - 2019-03-19
### Added
- initial version
